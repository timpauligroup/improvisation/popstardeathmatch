# Techrider für popstar deathmatch

Auf der Bühne:
  * bringen wir mit:
    * Controller
    * 2 Audio-Interfaces
    * 2 Laptops (brauchen Strom)
  * fordere ich an:
    * 2 Spotlights (muss auch nicht unbedingt sein, aber wäre cool)
    * 2 kleine Tische (können wir auch mitbringen)
    * 2 Stühle (können wir auch mitbringen)

1 Setup sehr weit links vorne auf der Bühne
1 Setup sehr weit rechts vorne auf der Bühne

Signalverlauf:
  * Audio-Interface links (2 Signale) (große Klinke) -> FOH (zusammenmischen)
  * Audio-Interface rechts (2 Signale) (große Klinke) -> FOH (zusammenmischen)
  * FOH (Signal von Audio-Interface links) -> linker Speaker auf Bühne
  * FOH (Signal von Audio-Interface rechts) -> rechter Speaker auf Bühne

Sonstiges:
  * ausreichend Kabel
  * ausreichend Mehrfachsteckdosen
